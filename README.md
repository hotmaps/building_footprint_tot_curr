[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4686939.svg)](https://doi.org/10.5281/zenodo.4686939)

# Building footprint density map of buildings in EU28 + Switzerland, Norway and Iceland for the year 2015


## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation
This dataset shows the building footprint of residential and non-residential buildings in EU28 on hectare (ha) level. 

The maps builds on two independent datasets. The first dataset is the European Settlement Map on the 10x10 m level [1], for which we applied a lower threshold which has be excceded in order to be counted.
The second dataset is the building layer of the OpenStreetMap (OSM) database [2]. 
For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.1.3 page 37.


### Limitations of the dataset
For the use and the estimation of the reliability of the data it is important to keep in mind that the data maps build on a statistical approach and do not take site specific or local conditions into account.
Manually performed data quality checks indicate that results are plausible on the hectare level of most regions. 

### References

[1] [European Settlement Map](https://land.copernicus.eu/pan-european/GHSL/european-settlement-map/esm-2012-release-2017-urban-green/view)

[2] [Open Street Map (OSM)](www.openstreetmap.org)


## How to cite
Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Mueller (e-think), Michael Hartner (TUW), Tobias Fleiter, Anna-Lena Klingler, Matthias Ku¨hnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) 


## Authors
Andreas Mueller <sup>*</sup>

<sup>*</sup> [TU Wien, EEG](https://eeg.tuwien.ac.at/), Institute of Energy Systems and Electrical Drives, Gusshausstrasse 27-29/370, 1040 Wien


## License
Copyright © 2016-2019: Andreas Mueller
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

